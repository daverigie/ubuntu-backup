#!/usr/bin/env bash

# List of files/patterns to skip during backup
IGNORE_LIST=./rsync-homedir-excludes.txt

# Location where backup will be placed
BACKUPDIR=/home/$USER/.backup/

# Maximum size of file to backup
MAXSIZE=1G

# Synchronize home directory to backup dir
rsync -aP --exclude-from=$IGNORE_LIST --exclude=$BACKUPDIR  --max-size=$MAXSIZE /home/$USER/ $BACKUPDIR

