#!/usr/bin/env bash

# make list of installed packages
dpkg --get-selections > package_list

# backup-repositories
rsync -aP /etc/apt/sources.list ./sources.list
rsync -aP /etc/apt/sources.list.d ./sources.list.d

