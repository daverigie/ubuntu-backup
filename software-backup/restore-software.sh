#!/usr/bin/env bash

# Get root privileges
[ "$(whoami)" != "root" ] && exec sudo -- "$0" "$@"

# restore repositories
rsync -aP ./sources.list /etc/apt/sources.list 
rsync -aP ./sources.list.d /etc/apt/sources.list.d 

# setup
apt-get update && apt-get upgrade

# install from package list
dpkg --clear-selections && dpkg --set-selections < ./package_list
apt-get update && apt-get dselect-upgrade

# cleanup
apt-get autoremove



