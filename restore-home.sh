#!/usr/bin/env bash

# Location of backup
BACKUPDIR=/home/$USER/.backup/

# Synchronize backup dir to homedir
rsync $BACKUPDIR /home/$USER/

